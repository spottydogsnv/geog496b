import arcpy
import os

#input parcels with ownership
allParcels = r"C:\PennState\GEOG596A\Base.gdb\BLM_NV_SMA"

#output from Nearest Neighbors tool
neighborParcels = r"C:\PennState\GEOG596A\Base.gdb\BLM_NV_SMA_Neighbors"
#Final output
landLockedParcels = r"C:\PennState\GEOG596A\Base.gdb\LandLockedParcels_AllJurisdictions"
outDissolve = os.path.join(os.path.dirname(allParcels), "{name}_dissolve".format(name=os.path.basename(allParcels)))

publicParcels = ["Bureau of Indian Affairs",
                 "Bureau of Land Management",
                 "Bureau of Reclamation",
                 "City of Las Vegas",
   
                 "Clark County, NV",
                 "Fish and Wildlife Service",
                 "Forest Service",
                 "National Park Service",
                 "Nevada State",
                 "Water"]

privateParcels = ["Department of Defense",
                  "Department of Engery",
                  "Private"]
outFields = {"NAME": {"type": "TEXT"},
             "ACRES": {"type": "DOUBLE"},
             "NDOT_ADJACENT": {"type": "SHORT"},
             "CITY_LIMITS": {"type": "SHORT"}}

def deleteIdentical():
    # Delete identical parcels in the parcel feature class
    deleteField = ["Shape"]
    arcpy.DeleteIdentical_management(in_dataset=allParcels, fields=deleteField)

def dissolveParcels():

    #Dissolve parcels based on ownership
    dissolveName = ["NAME"]
    if arcpy.Exists(outDissolve):
        arcpy.Delete_management(outDissolve)
    arcpy.Dissolve_management(in_features=allParcels,
                              out_feature_class=outDissolve,
                              dissolve_field=dissolveName,
                              multi_part="SINGLE_PART")
def adjParcels():

    #Run arcmap "Nearest Neighbors" tool
    neighborFields = ["ObjectID", "NAME"]
    if arcpy.Exists(neighborParcels):
        arcpy.Delete_management(neighborParcels)
    arcpy.PolygonNeighbors_analysis(in_features=allParcels,
                                    out_table=neighborParcels,
                                    in_fields=neighborFields,
                                    both_sides="BOTH_SIDES")

def checkPolygons():

    # Create dictionary to check polygons
    fields = [f.name for f in arcpy.ListFields(allParcels)]
    fields.append("SHAPE@LENGTH")
    fields.append("SHAPE@")
    field_dict = {}

    for i,f in enumerate(fields):
        field_dict[f] = i

    perimeterDict = {}
    typeDict = {}

    with arcpy.da.SearchCursor(in_table=allParcels,
                               field_names=fields) as cursor:
        for row in cursor:
            perimeterDict[row[field_dict["OBJECTID"]]] = row[field_dict["SHAPE@"]]
            typeDict[row[field_dict["OBJECTID"]]] = {"NAME": row[field_dict["NAME"]],
                                                     "PERIMETER": row[field_dict["SHAPE@LENGTH"]],
                                                     "GEOMETRY": row[field_dict["SHAPE@"]]}
    del row, cursor
    print "typeDict complete"
    return typeDict

def adjacentParcels(type_dict = None):

    #Identify landlocked parcels and create new feature class (landlockedParcels)

    parcelFID = set([row[0] for row in arcpy.da.SearchCursor(neighborParcels, "src_OBJECTID")])

    parcelCategories = set([row[0] for row in arcpy.da.SearchCursor(allParcels, "NAME")])

    fields = [f.name for f in arcpy.ListFields(neighborParcels)]
    field_dict = {}

    for i,f in enumerate(fields):
        field_dict[f] = i

    # parcelDict = {}

    for parcel in parcelFID:

        # print "starting {0}".format(parcel)

        lengthDict = {}

        for category in parcelCategories:
            lengthDict[category] = 0

        where_clause = "src_OBJECTID = {0}".format(parcel)

        with arcpy.da.SearchCursor(in_table=neighborParcels,
                                   field_names=fields,
                                   where_clause=where_clause) as cursor:
            for row in cursor:
                lengthDict[row[field_dict["nbr_NAME"]]] += row[field_dict["LENGTH"]]

        type_dict[parcel].update({"LENGTH": lengthDict})

    print "type_dict complete!"

    if arcpy.Exists(landLockedParcels):
        arcpy.Delete_management(landLockedParcels)
    arcpy.CreateFeatureclass_management(out_path=os.path.dirname(landLockedParcels),
                                        out_name=os.path.basename(landLockedParcels),
                                        geometry_type="POLYGON")
    for newField in outFields.iterkeys():
        arcpy.AddField_management(in_table=landLockedParcels,
                                  field_name=newField,
                                  field_type=outFields[newField]["type"],
                                  )

    insert_fields = ["SHAPE@", "NAME"]
    cursor = arcpy.da.InsertCursor(in_table=landLockedParcels,
                                   field_names=insert_fields)

    for parcel in type_dict.iterkeys():
        if type_dict[parcel]["NAME"] in publicParcels and "LENGTH" in type_dict[parcel]:
            if round(type_dict[parcel]["LENGTH"]["Private"], 1) == round(type_dict[parcel]["PERIMETER"], 1):
                print "FID {0} is Land Locked".format(parcel)
                cursor.insertRow((type_dict[parcel]["GEOMETRY"], type_dict[parcel]["NAME"]))

if __name__ == "__main__":

    deleteIdentical()
    dissolveParcels()
    adjParcels()
    type_dict = checkPolygons()
    adjacentParcels(type_dict=type_dict)
