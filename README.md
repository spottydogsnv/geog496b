Created by Eric Ford (elf24@psu.edu), Penn State GEOG 596B, Fall 2020

Purpose: identify landlocked parcels in statewide data using perimeter

Requirements: arcpy for analysis
parcels with ownership information